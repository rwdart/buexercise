﻿using Moq;
using Pkruczek.Exercise.Calculations.Calculation;
using Pkruczek.Exercise.Calculations.Configuration;
using Pkruczek.Exercise.Calculations.Transformations;
using Pkruczek.Exercise.Calculations.Validation;
using System;
using Xunit;

namespace Pkruczek.Exercise.CalculationsTest.Calculation
{
    public class CalculationServiceTests
    {
        ICalculationService _calculationService;

        public CalculationServiceTests()
        {
            var mockV = new Mock<IValidationService>();
            var mockT = new Mock<ITransformationsService>();
            _calculationService = new CalculationService(mockV.Object, mockT.Object);
        }

        [Theory]
        [InlineData(new char[] { '+', '-', '/', '*' }, "4+5*2", 14)]
        [InlineData(new char[] { '+', '-', '/', '*' }, "4+5/2", 6.5)]
        [InlineData(new char[] { '+', '-', '/', '*' }, "4+5/2-1", 5.5)]
        //;)
        [InlineData(new char[] { '+', '-', '/', '*' }, "2+258-1", 259)]
        [InlineData(new char[] { '+', '-', '/', '*' }, "2+2*22", 46)]
        [InlineData(new char[] { '+', '-', '/', '*' }, "2+2*2/4", 3)]
        [InlineData(new char[] { '+', '-', '/', '*' }, "2+2/4", 2.5)]
        [InlineData(new char[] { '+', '-', '/', '*' }, "2/6+2/4", 0.833333333333333)]
        [InlineData(new char[] { '+', '-', '/', '*' }, "2-5", -3)]
        public void Check_Calculations(char[] config, string value, double expect)
        {
            var configuration = new ConfigurationService();
            var transformation = new TransformationService(configuration);
            var validationService = new ValidationService(configuration);
            _calculationService = new CalculationService(validationService, transformation);
            configuration.SetAllowOperators(config);

            var data = _calculationService.Calculate(value);

            Assert.Equal(expect, data);
        }

        [Theory]
        [InlineData(new char[] { '+', '-', '/', '*' }, "2&2")]
        [InlineData(new char[] { '+', '-', '/', '*' }, "2+(2)")]
        [InlineData(new char[] { '+', '-', '/', '*' }, "2--2")]
        [InlineData(new char[] { '+', '-', '/', '*' }, "-2*2")]
        [InlineData(new char[] { '+', '-', '/', '*' }, "2*2-")]
        [InlineData(new char[] { '+', '-', '/', '*' }, "0.5+1")]
        [InlineData(new char[] { '-', '/', '*' }, "2+2")]
        [InlineData(new char[] { '+', '-', '/', }, "2*2")]
        public void If_WrongOperator_Should_Throw_Exception(char[] config, string value)
        {
            var configuration = new ConfigurationService();
            var transformation = new TransformationService(configuration);
            var validationService = new ValidationService(configuration);
            _calculationService = new CalculationService(validationService, transformation);
            configuration.SetAllowOperators(config);

            Action act = () => _calculationService.Calculate(value);

            Assert.Throws<Exception>(act);
        }

        [Theory]
        [InlineData(new char[] { '+', '-', '/', '*' }, "2/0")]
        [InlineData(new char[] { '+', '-', '/', '*' }, "2+2/0")]
        [InlineData(new char[] { '+', '-', '/', '*' }, "22/0*5")]
        public void If_DivByZero_Should_Throw_Exception(char[] config, string value)
        {
            var configuration = new ConfigurationService();
            var transformation = new TransformationService(configuration);
            var validationService = new ValidationService(configuration);
            _calculationService = new CalculationService(validationService, transformation);
            configuration.SetAllowOperators(config);

            Action act = () => _calculationService.Calculate(value);

            Assert.Throws<Exception>(act);
        }
    }
}
