﻿using Moq;
using Pkruczek.Exercise.Calculations.Configuration;
using Pkruczek.Exercise.Calculations.Transformations;
using System.Linq;
using Xunit;

namespace Pkruczek.Exercise.CalculationsTest.Transformation
{
    public class TransformationsServiceTests
    {
        ITransformationsService _transformationsService;

        public TransformationsServiceTests()
        {
            var mockConfiguration = new Mock<IConfigurationService>();
            _transformationsService = new TransformationService(mockConfiguration.Object);
        }

        [Theory]
        [InlineData(new char[] { '+', '-', '/', '*' }, "", 0)]
        [InlineData(new char[] { '+', '-', '/', '*' }, "2+258", 3)]
        [InlineData(new char[] { '+', '-', '/', '*' }, "2+2*22", 5)]
        [InlineData(new char[] { '+', '-', '/', '*' }, "2+2*22/9", 7)]
        public void Check_If_Collection_Has_Right_Quantity(char[] config, string value, int expect)
        {
            var configuration = new ConfigurationService();
            _transformationsService = new TransformationService(configuration);
            configuration.SetAllowOperators(config);

            var data = _transformationsService.ParseInputString(value);

            Assert.Equal(expect, data.Item1.Count());
            Assert.Equal(expect, data.Item2.Count());
        }

        [Theory]
        [InlineData(new char[] { '+', '-', '/', '*' }, new string[] { }, 0)]
        [InlineData(new char[] { '+', '-', '/', '*' }, new string[] { "2", "+", "258" }, 3)]
        [InlineData(new char[] { '+', '-', '/', '*' }, new string[] { "2", "+", "2", "+","22" }, 5)]
        [InlineData(new char[] { '+', '-', '/', '*' }, new string[] { "2", "+", "2", "*", "22", "/","9" }, 7)]
        [InlineData(new char[] { '+', '-', '/', '*' }, new string[] { "0.2", "+", "2", "*", "22", "/", "9" }, 7)]
        [InlineData(new char[] { '+', '-', '/', '*' }, new string[] { "0,5", "+", "2", "*", "22", "/", "9" }, 7)]
        public void Check_If_Collection_Has_Right_Quantity_ForList(char[] config, string[] value, int expect)
        {
            var configuration = new ConfigurationService();
            _transformationsService = new TransformationService(configuration);
            configuration.SetAllowOperators(config);

            var data = _transformationsService.ParseList(value.ToList());

            Assert.Equal(expect, data.Item1.Count());
            Assert.Equal(expect, data.Item2.Count());
        }
    }
}
