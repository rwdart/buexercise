﻿using Pkruczek.Exercise.Calculations.Models.Elements;
using Pkruczek.Exercise.Calculations.Operation;
using Pkruczek.Exercise.Calculations.Transformations.Mapping;
using System;
using Xunit;

namespace Pkruczek.Exercise.CalculationsTest.Transformation.Mapping
{
    public class ElementMappingExtensionsTest
    {
        [Fact]
        public void If_WrongOperator_Should_Throw_Exception()
        {
            Action act = () => "&".ToOperator(0);

            Assert.Throws<ArgumentException>(act);
        }

        [Theory]
        [InlineData("+", OperationOptionEnum.Add)]
        [InlineData("/", OperationOptionEnum.Divide)]
        [InlineData("*", OperationOptionEnum.Multiply)]
        [InlineData("-", OperationOptionEnum.Subtract)]
        public void CheckMainOperatorsMapping(string value, OperationOptionEnum expected)
        {
            var result = value.ToOperator(0);

            Assert.Equal(expected, result.OperationOption);
        }

        [Theory]
        [InlineData("+", 0, OperationPriorytyEnum.Medium)]
        [InlineData("*", 1, OperationPriorytyEnum.High)]
        [InlineData("/", 2, OperationPriorytyEnum.High)]
        [InlineData("-", 3, OperationPriorytyEnum.Medium)]
        public void CheckMainPropertyMapping(string value, int index, OperationPriorytyEnum expected)
        {
            var result = value.ToOperator(index);

            Assert.Equal(value, result.StringValue);
            Assert.Equal(index, result.Index);
            Assert.Equal(expected, result.Prioryty);
        }
    }
}
