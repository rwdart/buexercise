﻿using Moq;
using Pkruczek.Exercise.Calculations.Configuration;
using Pkruczek.Exercise.Calculations.Validation;
using System;
using Xunit;

namespace Pkruczek.Exercise.CalculationsTest.Validation
{
    public class ValidationServiceTest
    {
        IValidationService _validationService;

        public ValidationServiceTest()
        {
            var mockConfiguration = new Mock<IConfigurationService>();
            _validationService = new ValidationService(mockConfiguration.Object);
        }

        [Fact]
        public void If_InputNull_Should_Throw_Exception()
        {
            Action act = () => _validationService.CheckInputString(null);

            Assert.Throws<Exception>(act);
        }

        [Fact]
        public void If_InputEmpty_Should_Throw_Exception()
        {
            Action act = () => _validationService.CheckInputString("");

            Assert.Throws<Exception>(act);
        }

        [Theory]
        [InlineData(new char[] { '+', '-', '/', '*' }, "2&2")]
        [InlineData(new char[] { '+', '-', '/', '*' }, "2+(2)")]
        [InlineData(new char[] { '+', '-', '/', '*' }, "2--2")]
        [InlineData(new char[] { '+', '-', '/', '*' }, "-2*2")]
        [InlineData(new char[] { '+', '-', '/', '*' }, "2*2-")]
        [InlineData(new char[] { '+', '-', '/', '*' }, "0.5+1")]
        [InlineData(new char[] { '-', '/', '*' }, "2+2")]
        [InlineData(new char[] { '+', '-', '/', }, "2*2")]
        public void If_WrongOperator_Should_Throw_Exception(char[] config, string value)
        {
            var configuration = new ConfigurationService();
            _validationService = new ValidationService(configuration);
            configuration.SetAllowOperators(config);

            Action act = () => _validationService.CheckInputString(value);

            Assert.Throws<Exception>(act);
        }

        [Theory]
        [InlineData(new char[] { '+', '-', '/', '*' }, "2/0")]
        [InlineData(new char[] { '+', '-', '/', '*' }, "2+2/0")]
        [InlineData(new char[] { '+', '-', '/', '*' }, "22/0*5")]
        public void If_DivByZero_Should_Throw_Exception(char[] config, string value)
        {
            var configuration = new ConfigurationService();
            _validationService = new ValidationService(configuration);
            configuration.SetAllowOperators(config);

            Action act = () => _validationService.CheckInputString(value);

            Assert.Throws<Exception>(act);
        }
    }
}
