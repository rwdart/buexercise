﻿using Pkruczek.Exercise.Calculations.Configuration;
using System;
using System.Text.RegularExpressions;

namespace Pkruczek.Exercise.Calculations.Validation
{
    public class ValidationService : IValidationService
    {
        private readonly IConfigurationService _cofigurationService;

        public ValidationService(
            IConfigurationService cofigurationService
            )
        {
            _cofigurationService = cofigurationService;
        }

        public void CheckInputString(string input)
        {
            char[] operatorsConfig = _cofigurationService.GetAllowOperators();
            Regex peratorsPattern = _cofigurationService.GetMatchPattern();
            checkInputString(input);

            checkOperators(input, operatorsConfig);

            checkDivByZero(input, peratorsPattern);
        }

        private static void checkInputString(string input)
        {
            if (string.IsNullOrWhiteSpace(input))
                throw new Exception("Input Operation can't be empty");
        }

        private static void checkOperators(string input, char[] operatorsConfig)
        {
            var stringNumbers = input.Split(operatorsConfig);
            foreach (string snumeber in stringNumbers)
            {
                checkOperator(snumeber);
            }
        }

        private static void checkOperator(string snumeber)
        {
            int number;
            bool success = Int32.TryParse(snumeber, out number);
            if (!success)
                throw new Exception("Wrong operator used");
        }

        private static void checkDivByZero(string input, Regex pattern)
        {
            string last = null;
            foreach (var match in pattern.Matches(input))
            {
                string matchSring = match.ToString();

                if (last != null && last == "/" && matchSring == "0")
                    throw new Exception("Div by 0 detected");

                last = matchSring;
            }
        }
    }
}
