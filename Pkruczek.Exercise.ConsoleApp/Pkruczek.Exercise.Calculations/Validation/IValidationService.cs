﻿namespace Pkruczek.Exercise.Calculations.Validation
{
    public interface IValidationService
    {
        void CheckInputString(string input);
    }
}
