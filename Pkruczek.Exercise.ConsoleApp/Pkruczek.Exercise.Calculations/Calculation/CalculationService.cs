﻿using Pkruczek.Exercise.Calculations.Models.Elements;
using Pkruczek.Exercise.Calculations.Operation;
using Pkruczek.Exercise.Calculations.Transformations;
using Pkruczek.Exercise.Calculations.Validation;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Pkruczek.Exercise.Calculations.Calculation
{
    public class CalculationService : ICalculationService
    {
        private readonly IValidationService _validationService;
        private readonly ITransformationsService _transformationsService;

        public CalculationService(
            IValidationService validationService,
            ITransformationsService transformationsService
            )
        {
            _validationService = validationService;
            _transformationsService = transformationsService;
        }

        public double Calculate(string inputString)
        {
            _validationService.CheckInputString(inputString);

            var collections = _transformationsService.ParseInputString(inputString);

            while (collections.Item2.Count > 2)
            {
                Perform(collections);
                collections = _transformationsService.ParseList(collections.Item2);
            }

            double result = handleWrongResults(collections);

            return result;
        }

        public void Perform(Tuple<IEnumerable<IElement>, List<string>> collection)
        {
            IEnumerable<IOperator> operators = getOperators(collection.Item1);
            var operation = operators.OrderBy(a => a.Index).OrderBy(a => a.Prioryty).First();

            makeOperation(collection.Item2, operation);
        }

        private static IEnumerable<IOperator> getOperators(IEnumerable<IElement> collection)
        {
            return collection.Where(a => a.GetType() == typeof(OperatorModel))
                .Select(a => (OperatorModel)a);
        }

        private void makeOperation(List<string> collection, IOperator option)
        {
            IOperationStrategy operationStrategy = null;
            operationStrategy = getOperationOption(option.OperationOption);
            operationStrategy.Perform(collection, option);
        }

        private static IOperationStrategy getOperationOption(OperationOptionEnum option)
        {
            IOperationStrategy operationStrategy = null;
            switch (option)
            {
                case OperationOptionEnum.Multiply:
                    operationStrategy = new MultiplyOperation();
                    break;
                case OperationOptionEnum.Divide:
                    operationStrategy = new DivideOperation();
                    break;
                case OperationOptionEnum.Add:
                    operationStrategy = new AddOperation();
                    break;
                case OperationOptionEnum.Subtract:
                    operationStrategy = new SubtractOperation();
                    break;
                default:
                    break;
            }
            return operationStrategy;
        }

        private static double handleWrongResults(Tuple<IEnumerable<IElement>, List<string>> collections)
        {
            if (collections.Item2.Count > 1)
                throw new Exception("Somthing go wrong");

            string lastValue = collections.Item2.First();
            double result;
            if (!Double.TryParse(lastValue, out result))
                throw new Exception($"Can't convert result {lastValue} to double");
            return result;
        }
    }
}
