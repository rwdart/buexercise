﻿using Pkruczek.Exercise.Calculations.Models.Elements;
using System;
using System.Collections.Generic;

namespace Pkruczek.Exercise.Calculations.Calculation
{
    public interface ICalculationService
    {
        double Calculate(string inputString);
        void Perform(Tuple<IEnumerable<IElement>, List<string>> collection);
    }
}
