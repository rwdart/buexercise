﻿using Pkruczek.Exercise.Calculations.Models.Elements;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pkruczek.Exercise.Calculations.Operation
{
    interface IOperationStrategy
    {
        void Perform(List<string> collection, IOperator operation);
    }
}
