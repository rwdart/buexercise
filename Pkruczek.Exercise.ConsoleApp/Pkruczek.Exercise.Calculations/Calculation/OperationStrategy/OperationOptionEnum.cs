﻿using Pkruczek.Exercise.Calculations.Models.Elements;

namespace Pkruczek.Exercise.Calculations.Operation
{
    public enum OperationOptionEnum
    {
        Multiply,
        Divide,
        Add,
        Subtract,
    }

    public static class OperationOptionExtension
    {
        public static OperationPriorytyEnum GetPrioryty(this OperationOptionEnum value)
        {
            if (value == OperationOptionEnum.Multiply || value == OperationOptionEnum.Divide)
                return OperationPriorytyEnum.High;

            return OperationPriorytyEnum.Medium;
        }
    }
}
