﻿using System;
using System.Collections.Generic;

namespace Pkruczek.Exercise.Calculations.Operation.Extensions
{
    public static class OperationListExtensions
    {
        public static double GetDoubleByIndex(this List<string> collection, int index)
        {
            return collection[index].TryToDoubleParse();
        }

        public static void SetNewValue(this List<string> collection, int index, double result)
        {
            collection[index] = result.ToString();
        }

        public static void RemoveElement(this List<string> collection, int index)
        {
            collection.RemoveAt(index);
        }

        private static double TryToDoubleParse(this string value)
        {
            double number;
            if (!Double.TryParse(value, out number))
                throw new Exception($"Can't convert {value} to double");
            return number;
        }
    }
}
