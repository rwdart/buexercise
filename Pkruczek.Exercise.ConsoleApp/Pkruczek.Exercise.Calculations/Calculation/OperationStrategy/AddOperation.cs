﻿using Pkruczek.Exercise.Calculations.Models.Elements;
using Pkruczek.Exercise.Calculations.Operation.Extensions;
using System.Collections.Generic;

namespace Pkruczek.Exercise.Calculations.Operation
{
    class AddOperation : IOperationStrategy
    {
        public void Perform(List<string> collection, IOperator operation)
        {
            int indexX = operation.Index - 1;
            int indexY = operation.Index + 1;
            double numberX = collection.GetDoubleByIndex(indexX);
            double numberY = collection.GetDoubleByIndex(indexY);

            double result = numberX + numberY;

            collection.SetNewValue(indexY, result);
            collection.RemoveElement(operation.Index);
            collection.RemoveElement(indexX);
        }
    }
}
