﻿using Pkruczek.Exercise.Calculations.Models.Elements;
using Pkruczek.Exercise.Calculations.Operation;
using System;

namespace Pkruczek.Exercise.Calculations.Transformations.Mapping
{
    public static class ElementMappingExtensions
    {
        public static OperatorModel ToOperator(this string value, int index)
        {
            return new OperatorModel()
            {
                Index = index,
                StringValue = value,
                OperationOption = value.ToOperatorOption()
            };
        }

        private static OperationOptionEnum ToOperatorOption(this string value)
        {
            OperationOptionEnum operationType;
            switch (value)
            {
                case "*":
                    operationType = OperationOptionEnum.Multiply;
                    break;
                case "/":
                    operationType = OperationOptionEnum.Divide;
                    break;
                case "+":
                    operationType = OperationOptionEnum.Add;
                    break;
                case "-":
                    operationType = OperationOptionEnum.Subtract;
                    break;
                default:
                    throw new ArgumentException($"operator {value} not implemented");
            }

            return operationType;
        }

        public static ElementModel ToElement(this string value, int index)
        {
            return new ElementModel()
            {
                Index = index,
                StringValue = value
            };
        }
    }
}
