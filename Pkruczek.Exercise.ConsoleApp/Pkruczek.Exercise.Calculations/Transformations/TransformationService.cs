﻿using Pkruczek.Exercise.Calculations.Configuration;
using Pkruczek.Exercise.Calculations.Models.Elements;
using Pkruczek.Exercise.Calculations.Transformations.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Pkruczek.Exercise.Calculations.Transformations
{
    public class TransformationService : ITransformationsService
    {
        private readonly IConfigurationService _cofigurationService;

        public TransformationService(
            IConfigurationService cofigurationService
            )
        {
            _cofigurationService = cofigurationService;
        }

        public Tuple<IEnumerable<IElement>, List<string>> ParseInputString(string inputString)
        {
            Regex matchPatter = _cofigurationService.GetMatchPattern();

            var list = getMachedList(inputString, matchPatter);

            return ParseList(list.ToList());
        }

        public Tuple<IEnumerable<IElement>, List<string>> ParseList(List<string> inputChars)
        {
            var collection = new List<IElement>();
            var orginalCollection = new List<string>();
            string[] operators = _cofigurationService.GetOperators();

            parseElements(inputChars, orginalCollection, collection, operators);

            return new Tuple<IEnumerable<IElement>, List<string>>(collection, orginalCollection);
        }

        public IEnumerable<string> getMachedList(string inputString, Regex matchPatter)
        {
            foreach (var match in matchPatter.Matches(inputString))
            {
                yield return match.ToString();
            }
        }

        private static void parseElements(List<string> inputList, List<string> orginalCollection, List<IElement> collection, string[] operators)
        {
            int i = 0;
            foreach (var item in inputList)
            {
                parseElement(orginalCollection, collection, operators, i, item);
                i++;
            }
        }

        private static void parseElement(List<string> orginalCollection, List<IElement> collection, string[] operators, int i, string stringElement)
        {
            if (operators.Contains(stringElement))
                addOperator(collection, i, stringElement);
            else
                addElement(collection, i, stringElement);

            orginalCollection.Add(stringElement);
        }

        private static void addElement(List<IElement> collection, int i, string mstring)
        {
            collection.Add(mstring.ToElement(i));
        }

        private static void addOperator(List<IElement> collection, int i, string mstring)
        {
            collection.Add(mstring.ToOperator(i));
        }
    }
}
