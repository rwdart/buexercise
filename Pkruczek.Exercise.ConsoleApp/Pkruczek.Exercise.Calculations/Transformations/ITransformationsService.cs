﻿using Pkruczek.Exercise.Calculations.Models.Elements;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pkruczek.Exercise.Calculations.Transformations
{
    public interface ITransformationsService
    {
        Tuple<IEnumerable<IElement>, List<string>> ParseInputString(string inputString);
        Tuple<IEnumerable<IElement>, List<string>> ParseList(List<string> inputChars);
        
    }
}
