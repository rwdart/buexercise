﻿using System.Linq;
using System.Text.RegularExpressions;

namespace Pkruczek.Exercise.Calculations.Configuration
{
    public class ConfigurationService : IConfigurationService
    {
        private char[] allowOperators;

        public char[] GetAllowOperators()
        {
            return allowOperators;
        }

        public string[] GetOperators()
        {
            return allowOperators.Select(a => a.ToString()).ToArray();
        }

        public Regex GetMatchPattern()
        {
            return new Regex($@"([{escepeChars(allowOperators)}])|([0-9]+)");
        }

        public void SetAllowOperators(char[] operators)
        {
            allowOperators = operators;
        }

        private static string escepeChars(char[] operators)
        {
            return new string(operators).Replace("/", @"/\");
        }
    }
}
