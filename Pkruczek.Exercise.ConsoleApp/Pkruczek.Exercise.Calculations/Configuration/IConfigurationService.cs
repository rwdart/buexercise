﻿using System.Text.RegularExpressions;

namespace Pkruczek.Exercise.Calculations.Configuration
{
    public interface IConfigurationService
    {
        char[] GetAllowOperators();

        string[] GetOperators();

        Regex GetMatchPattern();

        void SetAllowOperators(char[] operators);
    }
}
