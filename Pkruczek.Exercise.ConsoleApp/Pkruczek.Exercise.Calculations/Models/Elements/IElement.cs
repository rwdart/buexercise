﻿namespace Pkruczek.Exercise.Calculations.Models.Elements
{
    public interface IElement
    {
        string StringValue { get; set; }
        int Index { get; set; }
    }
}
