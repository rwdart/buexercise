﻿using Pkruczek.Exercise.Calculations.Operation;

namespace Pkruczek.Exercise.Calculations.Models.Elements
{
    public enum OperationPriorytyEnum
    {
        High,
        Medium
    }

    interface IOperator : IElement
    {
        OperationOptionEnum OperationOption { get; set; }
        OperationPriorytyEnum Prioryty { get; set; }
    }
}
