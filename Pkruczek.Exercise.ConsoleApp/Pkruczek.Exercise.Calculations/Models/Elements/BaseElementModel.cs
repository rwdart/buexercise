﻿namespace Pkruczek.Exercise.Calculations.Models.Elements
{
    public abstract class BaseElementModel : IElement
    {
        public string StringValue { get; set; }
        public int Index { get; set; }
    }
}
