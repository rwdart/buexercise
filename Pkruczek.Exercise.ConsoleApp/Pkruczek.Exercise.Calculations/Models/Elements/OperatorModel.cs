﻿using Pkruczek.Exercise.Calculations.Operation;

namespace Pkruczek.Exercise.Calculations.Models.Elements
{
    public class OperatorModel : BaseElementModel, IOperator
    {
        private OperationOptionEnum _operationOption;
        public OperationOptionEnum OperationOption
        {
            get { return _operationOption; }
            set
            {
                _operationOption = value;
                Prioryty = value.GetPrioryty();
            }
        }

        public OperationPriorytyEnum Prioryty { get ; set; }
    }
}
