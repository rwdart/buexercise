﻿using System;

namespace Pkruczek.Exercise.ConsoleApp.UserInterface
{
    public class UserInterfacMessagesService : IUserInterfacMessagesService
    {
        private const string quitCommand = "q";

        public bool IsQuitCommand(string command)
        {
            return command.ToLower() == quitCommand;
        }

        public void ShowStartMessage()
        {
            writeLine("Hello Exercise!");
            writeLine("Please use numers and + - / * operators");
            writeLine($"Press {quitCommand} to quit");
        }

        public void LoopMessage()
        {
            writeLine("Please insert math operation:");
        }

        public void ResultMessage(string input, double result)
        {
            writeLine($"Operation: {input} = {result}");
            writeLine($"----------------------------------[Press {quitCommand} to quit]");
        }

        public void ExceptionMessage(Exception ex)
        {
            Console.WriteLine($"An Error: {ex.Message}");
            writeLine($"!---------------------------------[Press {quitCommand} to quit]");
        }

        private void writeLine(string message)
        {
            Console.WriteLine(message);
        }
    }
}
