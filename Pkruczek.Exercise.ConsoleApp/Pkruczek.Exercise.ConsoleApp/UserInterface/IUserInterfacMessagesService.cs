﻿using System;

namespace Pkruczek.Exercise.ConsoleApp.UserInterface
{
    public interface IUserInterfacMessagesService
    {
        bool IsQuitCommand(string command);
        void ShowStartMessage();
        void LoopMessage();
        void ResultMessage(string input, double result);
        void ExceptionMessage(Exception ex);
    }
}
