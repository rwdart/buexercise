﻿using Microsoft.Extensions.DependencyInjection;
using Pkruczek.Exercise.Calculations.Calculation;
using Pkruczek.Exercise.Calculations.Configuration;
using Pkruczek.Exercise.Calculations.Transformations;
using Pkruczek.Exercise.Calculations.Validation;
using Pkruczek.Exercise.ConsoleApp.UserInterface;
using System;

namespace Pkruczek.Exercise.ConsoleApp
{
    class Program
    {
        private static IServiceProvider _serviceProvider;

        static void Main(string[] args)
        {
            RegisterServices();

            var uiService = _serviceProvider.GetService<IUserInterfacMessagesService>();
            var configurationService = _serviceProvider.GetService<IConfigurationService>();
            var calculatorService = _serviceProvider.GetService<ICalculationService>();

            uiService.ShowStartMessage();

            SetConfiguration(configurationService);

            WorkingLoop(uiService, calculatorService);

            DisposeServices();
        }

        private static void WorkingLoop(IUserInterfacMessagesService uiService, ICalculationService calculatorService)
        {
            while (true)
            {
                try
                {
                    uiService.LoopMessage();

                    var inputLine = Console.ReadLine();

                    if (uiService.IsQuitCommand(inputLine))
                        break;

                    double result = calculatorService.Calculate(inputLine);

                    uiService.ResultMessage(inputLine, result);
                }
                catch (Exception ex)
                {
                    uiService.ExceptionMessage(ex);
                }
            }
        }

        private static void SetConfiguration(IConfigurationService configurationService)
        {
            configurationService.SetAllowOperators(new char[] { '+', '-', '/', '*' });
        }

        private static void RegisterServices()
        {
            var serviceCollection = new ServiceCollection();

            SetupServices(serviceCollection);

            _serviceProvider = serviceCollection.BuildServiceProvider();
        }

        private static void SetupServices(ServiceCollection serviceCollection)
        {
            serviceCollection.AddSingleton<IUserInterfacMessagesService, UserInterfacMessagesService>();
            serviceCollection.AddSingleton<IConfigurationService, ConfigurationService>();
            serviceCollection.AddSingleton<IValidationService, ValidationService>();
            serviceCollection.AddSingleton<ITransformationsService, TransformationService>();
            serviceCollection.AddSingleton<ICalculationService, CalculationService>();
        }

        private static void DisposeServices()
        {
            if (_serviceProvider == null)
            {
                return;
            }
            if (_serviceProvider is IDisposable)
            {
                ((IDisposable)_serviceProvider).Dispose();
            }
        }
    }
}
